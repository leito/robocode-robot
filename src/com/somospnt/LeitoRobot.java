package com.somospnt;

import java.awt.Color;
import java.util.Random;
import robocode.*;

public class LeitoRobot extends Robot {

    private Behaviour behaviour = Behaviour.FLEE;
    private boolean onWall = false;
    private int others = -1;
    private Random random = new Random();

    @Override
    public void run() {
        setColors(Color.black, Color.red, Color.white, Color.red, Color.darkGray);
        others = getOthers(); //cache others, seems to cause problem calling it many times

        while (true) {
            flee();
            setBehaviour();
        }
    }

    public void setBehaviour() {
        if (behaviour == Behaviour.FLEE && others < 3) {
            behaviour = Behaviour.ATTACK;
        }
    }

    public void flee() {
        ahead(9000);
    }

    @Override
    public void onRobotDeath(RobotDeathEvent event) {
        others--;
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        if (!onWall) {
            onWall = true;
            turnGunLeft(90);
        }
        turnRight(event.getBearing() - 90);
    }

    @Override
    public void onHitRobot(HitRobotEvent event) {
        back(random.nextInt(60) + 30);
        turnLeft(random.nextInt(150) + 25);
    }

    @Override
    public void onHitByBullet(HitByBulletEvent event) {
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
//        if (behaviour == Behaviour.ATTACK) {
            fire(2);
//        }
    }
}

enum Behaviour {
    FLEE,
    ATTACK
}
